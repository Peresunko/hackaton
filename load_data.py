__author__ = "Pavel Peresunko, Soroka Eugenia, Andrey Kharitonov and Andrey Stebenkov"
__copyright__ = "Copyright 2017, Untitled"
__credits__ = ["Pavel Peresunko", "Soroka Eugenia", "Andrey Kharitonov", "Andrey Stebenkov"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "peres94@yandex.ru"

import os
import pandas as pd
import numpy as np
import datetime

os.chdir("C:\\Users\\paul\\Documents\\Work\\nns")

def get_history(path):
    path = "history.csv"
    history = pd.read_csv(path, encoding = "UTF-8", delimiter=";").as_matrix()
    day_week = np.zeros(history.shape[0])
    
    for i in range(0, history.shape[0]):
        try:
            history[i, 0] = datetime.datetime.strptime(history[i, 0], "%d.%m.%Y")
            day_week[i] = history[i, 0].weekday()
        except:
            print(history[i, :])
            
    day_week = day_week.tolist()
    for i in range(1, len(day_week)):
        day_week[i] = int(day_week[i])

    history[:, 1] = np.array([str(x) for x in history[:, 1]])
    channels = list(np.unique(history[:, 1]))
    
    X = []
    start = 0
    
    while True:
        userList = {}
        try:
            start = day_week[start:].index(0) + start
            sec = day_week[start:].index(1) + start
            last = day_week[sec:].index(0) + sec - 1
        except:
            break
        if (last == 0):
            break
        for i in range(start, last):
            if history[i, 2] not in userList:
                userList[history[i, 2]] = np.zeros((7, len(channels) * 2))
            userList[history[i, 2]][day_week[i], channels.index(history[i, 1])] = history[i, 4]
            userList[history[i, 2]][day_week[i], len(channels) + channels.index(history[i, 1])] = history[i, 5]
        for i in userList:
            x = np.zeros((7, len(channels) * 2 + 1))
            for j in range(0, 7): x[j] = i
            x[:, 1:] = userList[i]
            X.append(x)
        
        start = last
    return X


def get_user_description(path):
    social = pd.read_csv(path, encoding = "UTF-8", delimiter=";")
    social = social.iloc[:, 0:7]
    
    # clear data
    social = social[social['Возраст (полных лет)'] != "нет данных"]
    social = social[social['Возраст (полных лет)'] != "0"]
    social = social[social['Возраст (полных лет)'] != "1"]
    social = social[social['Возраст (полных лет)'] != "2"]
    social = social[social['Пол'] != "нет данных"]
    social = social[social['Образование'] != "нет данных"]
    social = social[social['Регион'] != "нет данных"]
    social = social[social['Регион'] != "0"]
    social = social[social['Тип НП'] != "нет данных"]
    social = social[social['Тип НП'] != "0"]
    
    # generating ages group
    for i in range(0, social.shape[0]):
        if int(social.iloc[i, 2]) in range(0, 8):
            social.iloc[i, 2] = '0'
        if int(social.iloc[i, 2]) in range(8, 13):
            social.iloc[i, 2] = '1'
        if int(social.iloc[i, 2]) in range(13, 17):
            social.iloc[i, 2] = '2'
        if int(social.iloc[i, 2]) in range(17, 22):
            social.iloc[i, 2] = '3'
        if int(social.iloc[i, 2]) in range(22, 36):
            social.iloc[i, 2] = '4'
        if int(social.iloc[i, 2]) in range(36, 60):
            social.iloc[i, 2] = '5'
        if int(social.iloc[i, 2]) in range(60, 75):
            social.iloc[i, 2] = '6'
        if int(social.iloc[i, 2]) in range(75, 140):
            social.iloc[i, 2] = '7'
    
    age = list(np.unique(social.iloc[:, 2]))
    sex = list(np.unique(social.iloc[:, 3]))
    education = list(np.unique(social.iloc[:, 4]))
    region = list(np.unique(social.iloc[:, 5]))
    city_type = list(np.unique(social.iloc[:, 6]))
    users = {}
    # generate vector representation of user
    for i in range(0, social.shape[0]):
        user = social.iloc[i, :]
        age_t = np.zeros(len(age))
        age_t[age.index(user[2])] = 1
        sex_t = np.zeros(len(sex))
        sex_t[sex.index(user[3])] = 1  
        education_t = np.zeros(len(education))
        education_t[education.index(user[4])] = 1     
        region_t = np.zeros(len(region))
        region_t[region.index(user[5])] = 1   
        city_type_t = np.zeros(len(city_type))
        city_type_t[city_type.index(user[6])] = 1
        user_vector = np.concatenate((age_t, sex_t, education_t, region_t, city_type_t))
        users[user[0]] = user_vector
    
    return users

def test_results(real, model):
    score = np.zeros(6)
    if (np.argmax(real[0:1]) == np.argmax(model[0:1])):
        score[0] = 1
    else:
        score[0] = 0
    if (np.argmax(real[2:8]) == np.argmax(model[2:8])):
        score[1] = 1
    else:
        score[1] = 0
    if (np.argmax(real[9:16]) == np.argmax(model[9:16])):
        score[3] = 1
    else:
        score[3] = 0   
    if (np.argmax(real[17:20]) == np.argmax(model[17:20])):
        score[4] = 1
    else:
        score[4] = 0       
    if (np.argmax(real[21:28]) == np.argmax(model[21:28])):
        score[5] = 1
    else:
        score[5] = 0 
        
    return score

