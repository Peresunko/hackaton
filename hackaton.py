__author__ = "Pavel Peresunko, Soroka Eugenia, Andrey Kharitonov and Andrey Stebenkov"
__copyright__ = "Copyright 2017, Untitled"
__credits__ = ["Pavel Peresunko", "Soroka Eugenia", "Andrey Kharitonov", "Andrey Stebenkov"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "peres94@yandex.ru"

import numpy as np
from keras.models import Sequential  
from keras.layers.core import Dense
from keras.layers import Dropout
from keras.layers.recurrent import LSTM
import pandas as pd
from keras.layers.core import Activation 
from keras.models import model_from_yaml
from sklearn.model_selection import train_test_split
from load_data import get_history, get_user_description, test_results


# creating dataset
# list "history" consist of user's history for one week
# For example, first element of history is table with size 7 x 1148 + 1
# 7 - number of days, 1148 - number of channels multyplied by 2
# So each column in history represent channel.
# let's assume that we are watching at first history with 2 channels for 4 days
# table: history[0]
# IN_ID | ch1_dur | ch2_dur | ch1_slot | ch2_slot |
#   1   |   1000  |   100   |   10     |    1     |
#   1   |   200   |   1000  |    2     |    5     |
#   1   |   100   |   10    |    1     |    1     |
#   1   |   111   |   999   |    3     |    10    |
# This table means that user with index 1 watched channel ch1 for 1000 seconds,
# and switched to it for 10 times for first day. Also he watched ch2 for 100 seconds,
# and switched to in for 1 time.
# In second day he watched ch1 for 200 seconds, switched on it for 2 times ans so on.

history = get_history('history.csv')

# Dictionary users represent information about users.
# key - user index, value - array.
# firs 9 values - age of user. We used sovet system of ages. 
# 10000000 - user from 3 to 7 years old.
# 01000000 - 8-13
# 00100000 - 13-16
# 00010000 - 17-21
# 00001000 - 22-36
# 00000100 - 36-60
# 00000010 - 60-75
# 00000001 - 75-...
# Next 2 values - sex. 10 -female, 01- male
# Next 7 valied - education:
# 1000000 - Высшее (окончил ВУЗ с получением диплома/ два ВУЗа)',
# 0100000 - 'Незаконченное среднее/начальное',
# 0010000 - 'Неполное высшее (окончил 3-4 курса ВУЗа без получения диплома)',
# 0001000 - 'Отказ от ответа / ЗО',
# 0000100 - 'Среднее',
# 0000010 - 'Среднее специальное',
# 0000001 - 'Ученая степень (окончил аспирантуру)'
# next 9 values - region:
# 010000000 - 'Приволжский федеральный округ',
# 001000000 - 'Северо-Западный федеральный округ',
# 000100000 - 'Северо-Кавказский федеральный округ',
# 000010000 - 'Сибирский федеральный округ',
# 000001000 - 'Ульяновская область',
# 000000100 - 'Уральский федеральный округ',
# 000000010 - 'Центральный федеральный округ',
# 000000001 - 'Южный федеральный округ'
# next 4 values - city type:
# 1000 - 'Город с населением больше 400к',
# 0100 - 'Город с населением больше 45к, но меньше 400к',
# 0010 - 'Город с населением меньше 45к',
# 0001 - 'Сельский населенный пункт'
# 
users = get_user_description('social.csv')

# generate trainig data for neural network
X = []
Y = []
k = 0
for i in range(0, len(history)):
    try:
        h = history[i]
        if int(h[0, 0]) in users:
            X.append(h[:, 1:])
            Y.append(users[h[0, 0]])
    except:
        k = k + 1
        

tx = np.vstack(X)
ty = np.vstack(Y)
# len(X) - object count, X[0].shape[1] =  7 - day count, X[1].shape[1] -  user's wathes
tx1 = tx.reshape(len(X), X[0].shape[0], X[0].shape[1])
# seperate train data on train and test. As we have seperate test data,
# we can use all data for training
X_train, X_test, y_train, y_test = train_test_split(tx1, ty, test_size=0.0, random_state=42)

# creating and training neural network
model = Sequential()
# network consist of 30 neurons, input shape - matrix (7 days and 1148 channels columns)
model.add(LSTM(30, input_shape=(X[0].shape[0], X[0].shape[1])))
model.add(Dropout(0.2))
# 29 - number of characteristics
model.add(Dense(29, activation='linear'))
model.add(Activation("linear"))
model.compile(loss="mean_squared_error", optimizer="rmsprop")
# train (you can skip this iperation)
model.fit(X_train, y_train, nb_epoch=5, batch_size=100, verbose=1)

# save model
model_yaml = model.to_yaml()
with open("model_half.yaml", "w") as yaml_file:
    yaml_file.write(model_yaml)
model.save_weights("model_half.h5")

# load model from file
yaml_file = open('model.yaml', 'r')
loaded_model_yaml = yaml_file.read()
yaml_file.close()
loaded_model = model_from_yaml(loaded_model_yaml)
loaded_model.load_weights("model.h5")
loaded_model.compile(loss="mean_squared_error", optimizer="rmsprop")


# load history
history = get_history('history.csv')
# users_all have the same structire, as users
users_all = get_user_description('social_all.csv')
users = get_user_description('social.csv')

# delete users that was in training data
for i in users:
    del users_all[i]
# create data for test
X_test = []
Y_test = []
for i in range(0, len(history)):
    try:
        h = history[i]
        if int(h[0, 0]) in users_all:
            X_test.append(h[:, 1:])
            Y_test.append(users_all[h[0, 0]])
    except:
        pass

tx_test = np.vstack(X_test)
ty_test = np.vstack(Y_test)
tx1 = tx_test.reshape(len(X_test),  X_test[0].shape[0], X_test[0].shape[1])
# seperate data on train and test. As we want to test, we set test size to 0
X_train_test, X_test, y_train_test, y_test = train_test_split(tx1, ty_test, test_size=0.0, random_state=42)

# checking result on test data
# use loaded_model.predict(X_train_test) if you loaded model withoun trainig
res = model.predict(X_train_test)
results = []
for i in range(0, X_train_test.shape[0]):
    results.append(test_results(y_train_test[i], res[i]))
res = np.vstack(results)
# now res represent quality of presiction
# each row represent accurasy for a user.
# If prediction was right, it has value 1
# each row has 6 values. First value - age, 2 - sex, 3 - education, 4 - regiion, 5 - city_type
# for example, if first row is 111111, it means, that it was all right
# if row is 0111111, age was predicted wrong

# getting test accuracy
age_accuracy = np.sum(res[:,0]) / res.shape[0]
sex_accuracy = np.sum(res[:,1]) / res.shape[0]
education_accuracy = np.sum(res[:,2]) / res.shape[0]
region_accuracy = np.sum(res[:,3]) / res.shape[0]
city_type_accuracy = np.sum(res[:,4]) / res.shape[0]
print(age_accuracy,sex_accuracy, education_accuracy, region_accuracy, city_type_accuracy)
